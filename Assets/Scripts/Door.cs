﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    // Animator, bool, float

    public ObjectSelector objectSelectorScript;

    public DoorEmission doorEmissionLeft;
    public DoorEmission doorEmissionRight;

    void Start()
    {
        
    }
    
    void Update()
    {
        
    }

    public void LeftDoorSelected()
    {
        doorEmissionLeft.Selected();
    }

    public void LeftDoorUnselected()
    {
        doorEmissionLeft.Unselected();
    }

    public void RightDoorSelected()
    {
        doorEmissionRight.Selected();
    }

    public void RightDoorUnselected()
    {
        doorEmissionRight.Unselected();
    }
}
