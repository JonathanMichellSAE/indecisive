﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomGeneration : MonoBehaviour
{
    public List<Color> listOfColours = new List<Color>();
    public Color myColours;
    public MeshRenderer doorMesh;

    public List<Sprite> listOfSprites = new List<Sprite>();
    public Sprite mySprite;
    
    // Start is called before the first frame update
    void Start()
    {
        GenerateColour();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GenerateColour()
    {
        doorMesh = GetComponentInChildren<MeshRenderer>(); // Finding the mesh renderer on this component

        listOfColours.Clear(); // Clearing the list of previous colours

        // All our colours
        listOfColours.Add(Color.red);
        listOfColours.Add(Color.yellow);
        listOfColours.Add(Color.green);
        listOfColours.Add(Color.blue);
        listOfColours.Add(Color.magenta);
        listOfColours.Add(Color.white);
        listOfColours.Add(Color.black);

        myColours = listOfColours[Random.Range(0, listOfColours.Count)]; // Generating a random colour from list of colours

        doorMesh.material.color = myColours; // Applying the colour to our mesh renderer
    }

    public void ChangeDoorColour()
    {

    }

    public void GenerateShape()
    {

    }

    public void ChangeDoorShape()
    {

    }
}
