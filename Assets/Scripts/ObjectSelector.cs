﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ObjectSelector : MonoBehaviour
{
    public Door doorScript;
    
    //public bool isHovered = false;
    public bool isHoveredLEFT = false;
    public bool isHoveredRIGHT = false;

    //private List<GameObject> listOfGameObjects;
    public GameObject theLeftDoor;
    public GameObject theRightDoor;

    public DoorEmission doorEmissionLeft;
    public DoorEmission doorEmissionRight;

    //private void Awake()
    //{
    //    AddGameObjectsToList();
    //}

    void Start()
    {
        
    }

    void Update()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); // Using a raycast from the main camera to the mouses position

        if(Physics.Raycast(ray, out hit, 100.0f)) // If the raycast hits something then..
        {
            if(hit.transform != null)
            {
                if (hit.transform.gameObject == theLeftDoor)
                {
                    isHoveredLEFT = true; // For debugging
                    doorEmissionLeft.Selected();
                    //doorScript.LeftDoorSelected();
                    OnMouseDown(); // Might need to put this if statement in the function instead.
                }
                else
                {
                    isHoveredLEFT = false; // For debugging
                    doorEmissionLeft.Unselected();
                    //doorScript.LeftDoorUnselected();
                }

                if (hit.transform.gameObject == theRightDoor)
                {
                    isHoveredRIGHT = true; // For debugging
                    doorEmissionRight.Selected();
                    //doorScript.RightDoorSelected();
                    OnMouseDown(); // Might need to put this if statement in the function instead.
                }
                else
                {
                    isHoveredRIGHT = false; // For debugging
                    doorEmissionRight.Unselected();
                    //doorScript.RightDoorUnselected();
                }

                PrintName(hit.transform.gameObject); // Prints the name of the GameObject that is being hit from the raycast

                //if (listOfGameObjects.Contains(hit.transform.gameObject))
                //{
                //    isHovered = true;
                //}
                //else
                //{
                //    isHovered = false;
                //}
            }
        }
    }


    private void OnMouseDown()
    {
        // Selection made start transition!
        //Debug.Log("I clicked something happened...");

        if (gameObject == theLeftDoor)
        {
            Debug.Log("I clicked the LEFT door...");
        }

        if (gameObject == theRightDoor)
        {
            Debug.Log("I clicked the RIGHT door...");
        }
    }


    /// <summary>
    /// Prints the name of the GameObject
    /// </summary>
    /// <param name="go"></param>
    void PrintName(GameObject go)
    {
        //print(go.name);
    }

    public void LeftDoorSelected()
    {
        doorEmissionLeft.Selected();
    }

    public void LeftDoorUnselected()
    {
        doorEmissionLeft.Unselected();
    }

    public void RightDoorSelected()
    {
        doorEmissionRight.Selected();
    }

    public void RightDoorUnselected()
    {
        doorEmissionRight.Unselected();
    }

    //public void AddGameObjectsToList()
    //{
    //    listOfGameObjects.Add(theLeftDoor);
    //    listOfGameObjects.Add(theRightDoor);
    //}
}
