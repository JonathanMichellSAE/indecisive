﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectedObject : MonoBehaviour
{
    public GameObject selectedObject;
    public bool lookingAtObject = false;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(lookingAtObject == true)
        {
            // Do emmission colour
        }
    }

    private void OnMouseOver()
    {
        selectedObject = GameObject.Find(CastingToObject.selectedObject);
        lookingAtObject = true;

    }

    private void OnMouseExit()
    {
        lookingAtObject = false;
    }
}
