﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class DoorEmission : MonoBehaviour
{
    private Animator DoorAC;
    [SerializeField] private Color DoorEmissionColour;
    [SerializeField] private MeshRenderer DoorMesh;

    void Start()
    {
        DoorAC = GetComponent<Animator>();
    }

    void Update()
    {
        //DoorMesh.material.EnableKeyword("_EMISSION");
        //DoorMesh.material.SetColor("_EmissionColor", DoorEmissionColour);
    }

    public void Selected()
    {
        DoorAC.SetBool("IsSelected", true);
        DoDoorEmission();
    }

    public void Unselected()
    {
        DoorAC.SetBool("IsSelected", false);
        StopDoorEmission();
    }

    void DoDoorEmission()
    {
        DoorMesh.material.EnableKeyword("_EMISSION");
        DoorMesh.material.SetColor("_EmissionColor", DoorEmissionColour);
    }

    void StopDoorEmission()
    {
        DoorMesh.material.DisableKeyword("_EMISSION");
        //DoorMesh.material.SetColor(0, 0);
    }
}
